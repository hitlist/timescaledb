FROM postgres:10
ENV POSTGRES_VERSION=10
ENV TIMESCALEDB_VERSION 1.2.2
ENV PG_CRON_VERSION 1.1.4
ENV LANG en_US.utf8

RUN set -ex \
 apt-key adv --keyserver keyserver.ubuntu.com --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8 \                 
  && apt-get update \
  && apt-get -y install postgresql-common \                                                                           
                ca-certificates \                                                                                     
                build-essential libpq-dev locales wget unzip tree libkrb5-dev git \                                   
                openssl \                                                                                             
                libssl-dev \                                                                                          
                curl \                                                                                                
                python3 \                                                                                             
                python3-pip \                                                                                         
                tar \                                                                                                 
                make \                                                                                                
                cmake \                                                                                               
                postgresql-server-dev-${POSTGRES_VERSION} \
    && mkdir -p /build/timescaledb \                                                                                 
    && wget -O /timescaledb.tar.gz https://github.com/timescale/timescaledb/archive/$TIMESCALEDB_VERSION.tar.gz \    
    && tar -C /build/timescaledb --strip-components 1 -zxf /timescaledb.tar.gz \
    && rm -f /timescaledb.tar.gz \
    \                                                      
    && cd /build/timescaledb \                                                                                       
    && ./bootstrap \
    && cd build && make install \
    && cd ~ \
    \
    && rm -rf /build \
    && git clone https://github.com/theory/pgtap.git \
    && cd pgtap \
    && make && make install \
    && cd ~ \
    && rm -rf pgtap \
    && wget https://www.zombodb.com/releases/v10-1.0.3/zombodb_jessie_pg10-10-1.0.3_amd64.deb \
    && dpkg -i zombodb_jessie_pg10-10-1.0.3_amd64.deb \
    && wget -O /pg_cron.tgz https://github.com/citusdata/pg_cron/archive/v$PG_CRON_VERSION.tar.gz \
    && tar xvzf /pg_cron.tgz && cd pg_cron-$PG_CRON_VERSION \
    && sed -i.bak -e 's/-Werror//g' Makefile \
    && sed -i.bak -e 's/-Wno-implicit-fallthrough//g' Makefile \
    && make && make install \
    && cd ~ && rm -rf pg_cron.tgz && rm -rf pg_cron-* \
    && cd ~ \
    \
    && curl -s http://download.pipelinedb.com/apt.sh | bash \
    && apt-get install pipelinedb-postgresql-10 \
    && rm -rf /var/lib/apt/lists/* \
    && sed -r -i "s/[#]*\s*(shared_preload_libraries)\s*=\s*'(.*)'/\1 = 'timescaledb,pipelinedb,pg_cron,pg_stat_statements\2'/;s/,'/'/" /usr/share/postgresql/postgresql.conf.sample \
    && echo "cron.database_name = 'help'" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "wal_level = logical" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "max_replication_slots = 10" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "timescaledb.telemetry_level = off" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "max_worker_processes = 96" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "pipelinedb.num_combiners = 4" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "pipelinedb.num_workers = 4" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "pipelinedb.num_queues = 2" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "pipelinedb.num_reapers = 6" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "pipelinedb.ipc_hwm = 50" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "pipelinedb.matrels_writable = on" >> /usr/share/postgresql/postgresql.conf.sample \
    && echo "zdb.default_elasticsearch_url = 'http://elasticsearch:9200/'" >> /usr/share/postgresql/postgresql.conf.sa
mple

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
COPY migrations/* /docker-entrypoint-initdb.d/
COPY bin/* /usr/local/bin/
